<?php

/*
  Copyright (C) 2018 Raphaël . Droz + floss @ gmail DOT com
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version

  This is the "import side" of the blog posts migration.
  The export side is in the d7towxr_aeorgus.drush.inc file
  Sample use:
  wp @ae wxr-importer import /tmp/dump.wxr --verbose=debug --update-attachment-guids
*/

namespace DrupalImport;

class Migrator {
    // see WXR_CompositeAttachment in d7towxr.drush.inc
    const TEMP_UUID_META_KEY = "_wxr_import_temporary_uuid";
    const ORIG_FID = '_wxr_import_drupal_fid';
    const ORIG_NID = '_wxr_original_drupal_nid';
    const ORIG_URL = '_wxr_original_drupal_url';

    /* node where markup replacements have been done
       Since this operation depends upon the TEMP_UUID_META_KEY and ORIG_FID meta-key,
       this array store the post id so that meta can be cleaned-up afterward */
    public static $replacements = [];

    public static function init_common_hooks() {
        add_action( 'import_start', [ __CLASS__, 'alter_modified_date' ], 10);
        // hook applying to each <item>
        add_filter( 'wxr_importer.pre_process.post', [ __CLASS__, 'anticipate_attachment_guid' ], 10, 4);
        add_filter( 'wxr_importer.process_attachment.uploaded', [ __CLASS__, 'process_attachment_uploaded' ], 10, 4);
        // add_action( 'import_end', [ __CLASS__, 'clean_temporary_meta' ], 10);
    }

    public static function init_drupal_hooks() {
        add_action( 'wxr_importer.process_failed.post', [ __CLASS__, 'process_failed_post' ], 10, 5);
        add_action( 'wxr_importer.processed.post', [ __CLASS__, 'remap_inline_markup' ], 10, 5);
        add_action( 'wxr_importer.processed.post', [ __CLASS__, 'remap_featured_images' ], 15, 5);
    }

    public static function alter_modified_date() {
        // this function is run at WXR import start only (and not in every WP-cli command)
        add_filter( 'wp_insert_post_data', function ( $data , $postarr ) {
            $data['post_modified'] = $postarr['post_date'];
            $data['post_modified_gmt'] = $postarr['post_date_gmt'];
            return $data;
        }, 50, 2 );
    }

    /* When the --update-attachment-guids option is passed. A first run will change the GUID according to the download.
       So new import attempts using the GUID from the WXR file would fail.
       By hooking at `pre_process.post` and changing the URL to the predicted one, attachments previously imported will
       be detected and skipped rather than duplicated in subsequent imports */
    public static function anticipate_attachment_guid( $data, $meta, $comments, $terms ) {
        // Only if --update-attachment-guids was passed on
        $update_attachment_guids = \WP_CLI::get_runner()->assoc_args['update-attachment-guids'];
        if ( !$update_attachment_guids ) {
            return $data;
        }

        if ( $data['post_type'] !== 'attachment' ) {
            return $data;
        }

        $upload_subdir = date( 'Y/m', strtotime( $data['post_date'] ) );
        // see the below process_attachment_uploaded() function
        $data['guid'] = wp_upload_dir()['baseurl'] . '/' . $upload_subdir . '/' . sanitize_file_name( basename( urldecode( $data['guid'] ) ) );
        return $data;
    }


    /* Allow resuming download when importing using WordPress-Importer, see
       https://github.com/humanmade/WordPress-Importer/pull/152 */
    public static function process_attachment_uploaded( $uploaded, $remote_url, $post, $meta ) {
        $filepath = wp_upload_dir()['basedir'] . '/' . $post['upload_date'] . '/' . sanitize_file_name( basename( urldecode( $remote_url ) ) );
        if ( file_exists( $filepath ) && ($filesize = filesize( $filepath ))) {
            $wp_url = wp_upload_dir()['baseurl'] . '/' . $post['upload_date'] . '/' . urlencode( basename(  $filepath ) );
            $id = attachment_url_to_postid( $wp_url );
            if ( $id ) {
                // normally, post_exists() from WXR-importer would not even trigger the hook if attachment already exists.
                // still, implement the condition in case of
                $metadata = wp_get_attachment_metadata( $id );
                $metadata['url'] = $wp_url;
                \WP_CLI::debug( sprintf( 'unmanaged media file "%s" already registered (id: %d, %d bytes).',
                                         $post['post_title'], $id, $filesize ) );
                return $metadata;
            } else {
                \WP_CLI::debug( sprintf( 'unmanaged media file "%s" found on filesystem (%d bytes).',
                                         $post['post_title'], $filesize ) );
                return [ 'file' => $filepath, 'url' => $wp_url ];
            }
        }
        return $uploaded;
    }

    public static function process_failed_post( $post_id, $data, $meta, $comments, $terms ) {
        $parent = array_filter( array_map( function( $e ) { return $e['key'] === '_wxr_import_parent' ? $e['value'] : NULL; }, $meta ) );
        if( $parent ) {
            $parent = array_pop( $parent );
            \WP_CLI::warning( "For post at original location /node/" . intval( $parent ) );
        }
        foreach( $post_id->get_error_messages() as $m ) {
            \WP_CLI::warning( $m );
        }
    }

    public static function attachment_id_from_uuid($uuid, $verbose = TRUE, $data = []) {
        global $wpdb;
        $sql = $wpdb->prepare( "SELECT post_id FROM $wpdb->postmeta WHERE meta_key = '%s' AND meta_value = %s", self::TEMP_UUID_META_KEY, $uuid );
        $attachment_id = intval( $wpdb->get_var( $sql ) );
        if ( ! $attachment_id && $verbose ) {
            \WP_CLI::warning( sprintf( 'post % 4d: referenced media uuid %s not found, for post "%s" (%s)', $data['ID'], $uuid, $data['post_title'], $data['guid'] ) );
        }
        return $attachment_id;
    }

    public static function attachment_id_from_orig_fid($orig_fid, $verbose = TRUE, $data = []) {
        global $wpdb;
        $sql = $wpdb->prepare( "SELECT post_id FROM $wpdb->postmeta WHERE meta_key = '%s' AND meta_value = %s", self::ORIG_FID, $orig_fid );
        $attachment_id = intval( $wpdb->get_var( $sql ) );
        if ( ! $attachment_id && $verbose ) {
            \WP_CLI::warning( sprintf( 'post % 4d: referenced media (original fid %d) not found, for post "%s" (%s)', $data['ID'], $orig_fid, $data['post_title'], $data['guid'] ) );
        }
        return $attachment_id;
    }

    public static function post_id_from_drupal_nid($orig_nid) {
        global $wpdb;
        $sql = $wpdb->prepare( "SELECT post_id FROM $wpdb->postmeta WHERE meta_key = %s AND meta_value = %s", self::ORIG_NID, $orig_nid );
        return intval( $wpdb->get_var( $sql ) );
    }

    public static function post_id_from_drupal_url($orig_url) {
        global $wpdb;
        $sql = $wpdb->prepare( "SELECT post_id FROM $wpdb->postmeta WHERE meta_key = %s AND meta_value = %s", self::ORIG_URL, $orig_url );
        return intval( $wpdb->get_var( $sql ) );
    }

    private static function get_meta( $metas, $name ) {
        foreach( $metas as $m ) {
            if ( $m['key'] == $name ) {
                return $m['value'];
            }
        }
        return FALSE;
    }

    public static function remap_inline_markup( $post_id, $data, $meta, $comments, $terms ) {
        if ( ! in_array( $data['post_type'], [ 'post' ], TRUE ) ) {
            return FALSE;
        }

        if (! self::get_meta( $meta, '_wxr_import_has_attachment_refs' ) ) {
            return FALSE;
        }

        if ( ! preg_match( \WXR_Importer::REGEX_HAS_ATTACHMENT_REFS, $data['post_content'] ) ) {
            return NULL;
        }

        // extract managed/unmanaged ref
        // remap (change markup)
        $count = 0; $replacements = [];
        $c = preg_replace_callback('#class="(?:attachment-temp-(unmanaged-\w+|managed) )wp-image-(\d+)[^"]*" src=["\'](.*?)["\']#',
                                   function($matches) use ( $post_id, $data, &$replacements ) {
                                       // attachment was never given a fid in Drupal
                                       if ( strpos($matches[1], 'unmanaged-', 0) === 0 ) {
                                           if ( $matches[2] != '0000000' ) {
                                               var_dump("strange unmanaged attachment");
                                               print_r($matches);
                                               die;
                                           }

                                           $uuid = str_replace( 'unmanaged-', '', $matches[1] );
                                           $attachment_id = self::attachment_id_from_uuid($uuid, TRUE, ['ID' => $post_id] + $data);
                                           // print( "attachment replace $uuid => $attachment_id for {$data['post_id']}" . PHP_EOL );
                                       }
                                       else {
                                           // attachment had a fid (Drupal managed), which has been stored as a file meta
                                           $attachment_id = self::attachment_id_from_orig_fid($matches[2], TRUE, ['ID' => $post_id] + $data);
                                       }

                                       if ( !$attachment_id ) {
                                           return $matches[0];
                                       }

                                       // proceed with the file ID we found
                                       $replacements[] = $attachment_id;
                                       self::$replacements[] = $attachment_id;
                                       $parsed = parse_url( wp_get_attachment_url( $attachment_id ) );
                                       return sprintf( 'class="wp-image-%d" src="%s"', $attachment_id, $parsed["path"] );
                                   },
                                   $data['post_content'], -1, $count);

        if ( !$count ) {
            \WP_CLI::warning( sprintf( 'post % 4d: regexp mismatch, no replacement done (already replaced?), skipping', $post_id ) );
            return FALSE;
        }
        if ( !count( $replacements ) ) {
            \WP_CLI::warning( sprintf( 'post % 4d: no replacement could be done, skipping', $post_id ) );
            return FALSE;
        }

        if ( $count !== count( $replacements ) ) {
            \WP_CLI::warning( sprintf( 'post % 4d: Only %d/%d attachments references could be replaced', $post_id, count( $replacements ), $count ) );
        }

        $data['post_content'] = $c;
        $data['ID'] = $post_id;
        $result = wp_update_post( $data, true );

        if (! $result ) {
            \WP_CLI::warning( sprintf( "error saving substitued post (%d => %d)", $data['post_id'], $post_id ) );
            return FALSE;
        }

        return TRUE;
    }

    public static function remap_featured_images( $post_id, $data, $meta, $comments, $terms, $dry_run = FALSE ) {
        if ( ! in_array( $data['post_type'], [ 'post' ], TRUE ) ) {
            return NULL;
        }

        $ret = [];
        foreach(['_thumbnail_id', 'headerImage'] as $FIELD_NAME) {
            if (! ( $old_id = self::get_meta( $meta, $FIELD_NAME ) ) ) {
                continue;
            }

            $new_id = self::attachment_id_from_orig_fid($old_id, TRUE, ['ID' => $post_id] + $data);
            if ( $new_id ) {
                if ($dry_run) {
                    \WP_CLI::log( sprinf( "update_post_meta(%d, %s, %d => %d)", $post_id, $FIELD_NAME, $old_id, $new_id ) );
                } else {
                    $ret[] = update_post_meta( $post_id, $FIELD_NAME, $new_id );
                    \WP_CLI::log( sprintf( "post % 3d modified attached file field %s: ID from % 4d to % 4d", $post_id, $FIELD_NAME, $old_id, get_post_meta( $post_id, $FIELD_NAME, TRUE) ) );
                }
            }
        }
        return $ret ? : NULL;
    }

    public static function clean_temporary_meta() {
        // delete attachment meta key
        foreach( self::$replacements as $id ) {
            delete_post_meta( $id, self::TEMP_UUID_META_KEY );
            delete_post_meta( $id, self::ORIG_FID );
        }
        self::$replacements = [];
    }
}
