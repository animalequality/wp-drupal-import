The WordPress import counterpart of [drupal2wordpress](https://gitlab.com/animalequality/drupal2wordpress)

This provides a set of [WordPress-Importer](https://github.com/humanmade/WordPress-Importer) hooks which
allow better handling of custom meta-fields present in WXR files as generated by [drupal2wordpress](https://gitlab.com/animalequality/drupal2wordpress).
