<?php

/*
  Copyright (C) 2018, 2019 Raphaël . Droz + floss @ gmail DOT com
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version

  Tools for the Drupal import
*/

namespace DrupalImport;

use DrupalImport\Migrator;

class Tools extends WpHelpers
{

    private $dry_run = FALSE;

    /**
     * Run the migration URL remapping process for one or several posts.
     *
     * ## OPTIONS
     *
     * [--<field>=<value>]
     * : One or more args to pass to WP_Query.
     *
     * [--markup]
     * : The type of remapping to run.
     *
     * [--field-related]
     * : The type of remapping to run.
     *
     * [--attachments]
     * : Remapped attachment ID
     *
     * [--dry-run]
     * : Dry-run
     *
     */
    public function remap(  $args, $assoc_args ) {
        $query_args = self::mkquery( [ 'posts_per_page' => -1,
                                       'post_status' => 'any' ],
                                     $assoc_args,
                                     TRUE );
        $query = new \WP_Query( $query_args );
        $this->dry_run = !empty($query_args['dry-run']);

        if ( isset( $query_args['markup'] ) ) {
            array_walk( $query->posts, [ $this, 'remap_markup' ] );
        }
        if ( isset( $query_args['field-related'] ) ) {
            array_walk( $query->posts, [ $this, 'remap_related_content_field' ] );
        }
        if ( isset( $query_args['attachments'] ) ) {
            array_walk( $query->posts, [ $this, 'fix_attachments_id' ] );
        }
    }

    private function remap_markup( $post ) {
        $id = $post->ID;
        $meta = get_post_meta( $id );
        foreach( $meta as $m => $v ) {
            $meta2[] = [ 'key' => $m, 'value' => $v[0] ];
        }
        $replaced = Migrator::remap_inline_markup( $id, (array)$post, $meta2, [], [] );
        if ( $replaced ) {
            printf( "% 3d modified" . PHP_EOL, $id );
        }
        elseif( $replaced === NULL) {
            \WP_CLI::warning( sprintf( 'post %s: "%s" has one attachment not referenced inside post content markup.', $post->guid, $post->post_title ) );
        }
    }

    /* Imported thumbnail are in a high range to avoid collision.
       Basically original_ID + an hardcoded value.
       This function does the fix. IT'S NOT IDEMPOTENT. */
    private function fix_attachments_id( $post ) {
        $id = $post->ID;
        $meta = get_post_meta( $id );
        foreach( $meta as $m => $v ) {
            $meta2[] = [ 'key' => $m, 'value' => $v[0] ];
        }
        $modified = Migrator::remap_featured_images( $id, (array)$post, $meta2, [], [], $dry_run = $this->dry_run );
    }

    public function remap_related_content_field( $post ) {
        $id = $post->ID;
        $rc = $rc_save = get_field( 'related_contents', $id );
        if (! $rc ) return;

        $regexp = '^(?:(?:https?:)?//(?:www\.)?animalequality\.net)?/+(.*?)$';
        $match_count = $subst_count = 0;
        foreach($rc as $k => &$meta) {
            $url = &$meta['url']['url'];
            if (! preg_match('#' . $regexp . '#', $url)) {
                continue;
            }
            $url = preg_replace_callback('#' . $regexp . '#',
                                         function($matches) use(&$match_count, &$subst_count) {
                                             $remapped_url = '';
                                             $rm = [];
                                             if (preg_match('!(?:news|noticias)/([0-9]+)/!', $matches[1], $rm)) {
                                                 $match_count++;
                                                 $new_id = Migrator::post_id_from_drupal_nid( $rm[1] );
                                                 $remapped_url = get_permalink($new_id);
                                                 if ($remapped_url) {
                                                     $subst_count++;
                                                     return $remapped_url;
                                                 }
                                             }
                                             else {
                                                 $match_count++;
                                                 $new_id = Migrator::post_id_from_drupal_url( '/' . trim($matches[1], '/') );
                                                 $remapped_url = get_permalink($new_id);
                                                 if ($remapped_url) {
                                                     $subst_count++;
                                                     return $remapped_url;
                                                 }
                                             }
                                             printf("No substitution found for URL %s" . PHP_EOL, $matches[0]);
                                             return $matches[0];
                                         },
                                         $url, -1);
        }
        if (! $match_count) return;
        if ($subst_count) {
            $saved = update_field( 'related_contents', $rc, $id );
        } else {
            $saved = false;
        }

        printf("#### %d (%d/%d) %s" . PHP_EOL, $id, $subst_count, $match_count, $saved ? '[saved]' : '');
        if ($subst_count != $match_count) {
            print("\t" . trim(print_r($rc, TRUE)) . PHP_EOL . PHP_EOL);
        }
    }

    /**
     * Generate a <old url> => <new url> mapping of former Drupal file and node URLs.
     *
     * ## OPTIONS
     *
     * [--<field>=<value>]
     * : One or more args to pass to WP_Query.
     *
     * [--fields=<fields>]
     * : Limit the output to specific object fields.
     *
     * [--format=<format>]
     * : Render output in a particular format.
     * ---
     * default: table
     * options:
     *   - table
     *   - csv
     *   - ids
     *   - json
     *   - count
     *   - yaml
     * ---
     *
     * [--src-domain]
     * : (Only for files): Whether or not to prefix URLs with the old domain name
     *
     * [--dst-domain]
     * : Whether or not to prefix destination URLs with the new domain name
     *
     */
    public function redirects( $args, $assoc_args ) {
        $options = array_merge( [ 'src-domain' => TRUE,
                                  'dst-domain' => TRUE ], $assoc_args );

        $query_args = self::mkquery( [ 'posts_per_page' => -1,
                                       'orderby' => 'date',
                                       'order' => 'ASC',
                                       'post_status' => 'any',
                                       'meta_query' => [ 'relation' => 'OR' ] ],
                                     $assoc_args,
                                     FALSE );

        if ( ! isset( $query_args['post_type'] ) ) {
            return ;
        }

        if ( 'attachment' === $query_args['post_type'] || in_array( 'attachment', $query_args['post_type'] ) ) {
            $query_args['meta_query'][] = [ 'key' => self::ORIG_FILE_URL_METAKEY, 'compare' => 'EXISTS' ];
        }
        if ( 'attachment' !== $query_args['post_type'] ) { // any regular post type
            $query_args['meta_query'][] = [ 'key' => self::ORIG_NODE_URL_METAKEY, 'compare' => 'EXISTS' ];
        }

        $query = new \WP_Query( $query_args );
        array_walk( $query->posts, function( $node, $key, $options ) {
            if ( $node->post_type === 'attachment' ) {
                self::attachmentRedirects(  $node, $key, $options );
            }
            else {
                self::postRedirects(  $node, $key, $options );
            }
        }, $options );

        $formatter = $this->get_formatter( $assoc_args );
        $formatter->display_items( $query->posts );
    }

    private function postRedirects( $node, $key, $options ) {
        $src = get_post_meta( $node->ID, self::ORIG_NODE_URL_METAKEY, TRUE );
        $dst = get_permalink( $node );

        $node->dst_url = $dst;
        $node->src_url = $src;
        $node->src = $src;
        $node->dst = str_replace( home_url(), "", $dst );
    }

    private function attachmentRedirects( $node, $key, $options ) {
        $src = get_post_meta( $node->ID, self::ORIG_FILE_URL_METAKEY, TRUE );
        $dst = wp_get_attachment_url( $node->ID );

        $node->src_url = $src;
        $node->dst_url = $dst;
        $node->src = preg_replace( '!https?://[^/]+!', '', $src );
        $node->dst = str_replace( home_url(), "", $dst );
    }
}
