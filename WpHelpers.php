<?php

/*
  Copyright (C) 2018, 2019 Raphaël . Droz + floss @ gmail DOT com
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version

  Tools for the Animal Equality website(s)
*/

namespace DrupalImport;

class WpHelpers extends \WP_CLI\CommandWithDBObject
{
    protected $obj_type = 'post';

    protected $obj_fields = array(
        'ID',
        'post_date',
        'post_title'
    );

    public function __construct() {
        $this->fetcher = new \WP_CLI\Fetchers\Post;
    }

    const ORIG_FILE_URL_METAKEY = '_wxr_import_drupal_url';
    const ORIG_NODE_URL_METAKEY = '_wxr_original_drupal_url';

    protected static function mkquery( $defaults, $assoc_args, $mono_post_type = TRUE ) {
        $query_args = array_merge( $defaults, $assoc_args );
        $query_args = self::process_csv_arguments_to_arrays( $query_args );
        if ( ! $mono_post_type && isset( $query_args['post_type'] ) && 'any' !== $query_args['post_type'] ) {
            $query_args['post_type'] = explode( ',', $query_args['post_type'] );
        }
        return $query_args;
    }
}
