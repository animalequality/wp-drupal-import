<?php
/*
  Plugin Name: WordPress-Drupal import
  Plugin URI: https://github.com/animalequality/wp-drupal-import
  Description: The WordPress import counterpart of drupal2wordpress WXR exporter.
  Version: 0.1
  Author: Raphaël Droz
  Author URI: https://www.animalequality.org
  License: GPLv3 Copyright (c) 2018 drzraf
*/

/**
 * Copyright (C) 2018, 2019 Raphaël . Droz + floss @ gmail DOT com
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version
 */

use DrupalImport\Migrator;

if ( defined( 'WP_CLI' ) ) {
    if ( file_exists( __DIR__ . '/vendor/autoload.php' ) ) {
        require_once( __DIR__ . '/vendor/autoload.php' );
    }
    \WP_CLI::add_command( 'drupal-import', 'DrupalImport\Tools' );
    Migrator::init_common_hooks();
    Migrator::init_drupal_hooks();
}
